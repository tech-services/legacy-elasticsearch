from __future__ import absolute_import

VERSION = (0, 4, 5)
__version__ = VERSION
__versionstr__ = '.'.join(map(str, VERSION))

from legacy_elasticsearch.client import Elasticsearch
from legacy_elasticsearch.transport import Transport
from legacy_elasticsearch.connection_pool import ConnectionPool, ConnectionSelector, \
    RoundRobinSelector
from legacy_elasticsearch.serializer import JSONSerializer
from legacy_elasticsearch.connection import Connection, RequestsHttpConnection, \
    Urllib3HttpConnection, MemcachedConnection, ThriftConnection
from legacy_elasticsearch.exceptions import *

